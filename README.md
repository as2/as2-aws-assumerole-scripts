# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Before Executing ###

Please verify that a correct profile is present in ~/.aws/credentials (with as2-login suffix)

```
[gdurier-as2-login]
aws_access_key_id = <key>
aws_secret_access_key = <secret>
```

Name of profile would be the same as Username created by alphasquare

### Executing? ###

Usage: `source ./assumerole-with-mfa <TARGET_ACCOUNT_ID> <PROFILE>`

* <TARGET_ACCOUNT_ID>: Account sur lequel vous souhaitez vous connecter
* <PROFILE>: Le nom du profile doit être présent dans ~/.aws/credentials

### After Executing ###

You can verify with aws_cli command like

`aws s3 ls`
`aws ec2 describe-instances`
